using Documenter, GitlabJuliaDemo

makedocs(
    modules = [GitlabJuliaDemo],
    checkdocs = :exports,
    sitename = "GitlabJuliaDemo.jl",
    pages = Any["index.md"],
    repo = "https://gitlab.com/floswald/GitlabJuliaDemo.jl/blob/{commit}{path}#{line}"
)
